class ProductController < ApplicationController
  def index
    "Listado de Productos"
  end

  def show(id)
    "Mostrando Datos del Id: #{id}"
  end
end
